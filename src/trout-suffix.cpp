/*
	William Markley, NDBL
*/

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <libgen.h>
using namespace std;

struct Node{
	string path_label;
	vector<Node*> outgoing_nodes;  // at most 5 outgoing (ATCG$)
	Node(string s): path_label(s){}
};

Node* createSuffixTree(string);
void insertSuffix(Node*, string);
void insert(Node*, string);
bool needChildNode(Node*, string, int &);
bool needIntermediateNode(Node*, string &, string &, string &, int);
void addChild(Node*, string);
void addIntermediateTree(Node*, int, string, string, string);
void printTree(Node*, int);
void deleteSuffixTree(Node*);
bool foundSuffix(Node*,string);


int main(int argc, char** argv){

	if(argc!=4){
		cerr << "trout-suffix: Incorrect number of arguments\n";
		cerr << "\nusage:    trout-suffix <input_fastq> <kmer_markers> <output_sketch>\n";
		return 1;
	}

	ifstream infile(argv[1]);

	if(!infile){
		cerr << "trout-suffix: Unable to open argument 1\n";
		cerr << "\nusage:    trout-suffix <input_fastq> <kmer_markers> <output_sketch>\n";
		return 1;
	}

	ifstream markerFile(argv[2]);

	if(!markerFile){
		cerr << "trout-suffix: Unable to open argument 2\n";
		cerr << "\nusage:    trout-suffix <input_fastq> <kmer_markers> <output_sketch>\n";
		return 1;
	}

	ofstream sketch(argv[3], ofstream::app);

	if(!sketch){
		cerr << "trout-suffix: Unable to open argument 3\n";
		cerr << "\nusage:    trout-suffix <input_fastq> <kmer_markers> <output_sketch>\n";
		return 1;
	}

	string kmer;
	string searchstr;
	string identifier;

	// create Suffix tree
	getline(infile, identifier);
	getline(infile, kmer);
	getline(infile, identifier);
	getline(infile, identifier);
	Node* root = createSuffixTree(kmer);

	while(getline(infile, identifier)){
		getline(infile, kmer);
		getline(infile, identifier);
		getline(infile, identifier);

		insertSuffix(root, kmer);
	}

	//printTree(root,0);

	sketch << basename(argv[1]) << ":";

	while(getline(markerFile, searchstr)){
		// search Suffix tree for searchstr
		if(foundSuffix(root, searchstr)){
			sketch<<"1 ";
		}
		else{
			sketch<<"0 ";
		}
	}

	sketch << endl;

	// delete Suffix tree
	deleteSuffixTree(root);
	infile.close();
	markerFile.close();
	sketch.close();

	return 0;
}


Node* createSuffixTree(string suffix){
	suffix += '$';
	Node* root = new Node("");

	while(suffix.length() > 0){  // while the suffix is not empty 
	// place suffix in tree, and then remove first char of suffix
		insert(root, suffix);		
		suffix = suffix.substr(1,suffix.length()-1);  // SJE:  faster ways to do this, but this is most efficient code-wise
	}
	return root;
}

void insertSuffix(Node* root, string suffix){
  
  suffix += '$';

  while(suffix.length() > 0){   // SJE:  see above
    insert(root, suffix);
    suffix = suffix.substr(1,suffix.length()-1);
  }

}


void insert(Node* root, string suffix){
	string before="";
	string after="";
	int child_position=0;

	if(needChildNode(root, suffix, child_position)){
		addChild(root, suffix);
	}
	else if(needIntermediateNode(root, suffix, before, after, child_position)){
		addIntermediateTree(root, child_position, before, after, suffix);
	}
	else{  // hit a new node; begin process with this node
		root  = (root->outgoing_nodes)[child_position];
		insert(root, suffix);
	}
}


bool needChildNode(Node* root, string insertString, int &child_position){
  
  vector<Node*>::iterator currPos = root->outgoing_nodes.begin();

  for(int i = 0; currPos != root->outgoing_nodes.end(); currPos++){
    		
    if( insertString.at(0)== (*currPos)->path_label.at(0) ){
      child_position=i;
      return false;
    }
    i++;
  
  }

  return true;

}


bool needIntermediateNode(Node* root, string &suffix, string &before, string &after, int child_position){
  //vector<Node*> children = root->outgoing_nodes;
	string currLabel = root->outgoing_nodes[child_position]->path_label;
	
	// find out how far to go down the path
	for(int i=0; i<(int)currLabel.length(); i++){
	
		// check if suffix differs from path
		if(currLabel.at(i)!=suffix.at(i)){  // if it differs, we need an intermediate node at this point
			before = currLabel.substr(0,i);  // everything before difference
			after = currLabel.substr(i, (currLabel.length()-1));  // difference to end
			
			// add rest of suffix after intermediate node
			suffix = suffix.substr(i, (suffix.length()-1));  // difference to end
			return true;
		}
	}

	if (currLabel.length()==suffix.length()){  // currLabel and suffix are same string
		suffix = "";
		return true;
	}

	
	// no difference detected before end of path_label
	// implies we hit an intermediate node (i.e. children[child_position] is an intermediate node)
	// remove part of suffix we've traversed
	suffix = suffix.substr( currLabel.length(), suffix.length()-1 );  // one after end of path_label to the end of the suffix
	return false;
}


void addChild(Node* parent, string value){
	Node* child = new Node(value);
	(parent->outgoing_nodes).push_back(child);
}


void addIntermediateTree(Node* parent, int position, string before, string after, string remaining){
	if (remaining==""){  // suffix was equal to something already in the tree
		return;
	}


	Node* old = (parent->outgoing_nodes)[position];

	// create and link intermediate node
	Node* intermediate = new Node(before);                                      // create new node with before path label
	(old->path_label) = after;                                                  // modify path label of old node
	(intermediate->outgoing_nodes).push_back(old);                              // add old node to the intermediate node's children
	(parent->outgoing_nodes).erase((parent->outgoing_nodes).begin()+position);  // remove old node from the parent's children
	(parent->outgoing_nodes).push_back(intermediate);                           // add intermediate node to parent's children
	
	// add rest of suffix to the new intermediate node
	addChild(intermediate, remaining);
}

void printTree(Node* root, int indent){
	for(int i=0; i<indent; i++){
		cout << "\t";
	}
	cout <<">"<<(root->path_label)<<"<" << endl;

	for(int i=0; i<(int)(root->outgoing_nodes).size(); i++){
		printTree( (root->outgoing_nodes)[i], indent+1);
	}
}

void deleteSuffixTree(Node* root){
	for(int i=0; i<(int)(root->outgoing_nodes).size(); i++){
                deleteSuffixTree( (root->outgoing_nodes)[i]);
        }
	delete root;
}


bool foundSuffix(Node* root,string searchstr){
  int position;  

	if(needChildNode(root, searchstr, position)){  // not found in child
		return false;
	}

	string currLabel = root->outgoing_nodes[position]->path_label;
	
	int i;
	int depth = (searchstr.length() < currLabel.length()) ? searchstr.length() : currLabel.length();

	// find out how far to go down the path
	for(i=0; i<depth; i++){

	  // check if searchstr differs from path
	  if(currLabel.at(i)!=searchstr.at(i)){  // if it differs return false
	    return false;
	  }
	
	}

	// no differences detected in path; hit a node
	// if searchstr length is same as path label
	if(i == (int)searchstr.length()){
		return true;  // no differences found; search is at least a prefix of path
	}
	else{  // hit node but more searchstr is left
		searchstr = searchstr.substr( currLabel.length(), searchstr.length()-1 );  // one after end of path_label to the end of the search str
		return foundSuffix( root->outgoing_nodes[position], searchstr);
	}
}
